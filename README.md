# Odfgenerator

## What is this?
Odfgenerator - wrapper for unzip, zip and libreoffice (headless mode) which generate ODT, DOC, DOCX and PDF documents by ODT template document

## Deployment

See `odfgenerator/Dockerfile`, `docker-compose.yaml` and `odfgenerator/src/app_config.js` files for deployment details.

Then:

`./install.sh`

## Selfremoving

Uninstalling with save `node_modules` directory in `ffmpeg-service/src`:

`./uninstall.sh`

Full uninstalling:

`./uninstall-full.sh`

## Example usage 1 (curl)

`cd odfgenerator/upload_test/template`

`./upload.sh`

Then see result files in directory `odfgenerator/upload_test/template`

See file `odfgenerator/upload_test/template/upload.sh` for http-query details

## Example usage 2 (curl)

`cd odfgenerator/upload_test/zayavlenie_pk`

`./upload.sh`

Then see result files in directory `odfgenerator/upload_test/zayavlenie_pk`

See file `odfgenerator/upload_test/zayavlenie_pk/upload.sh` for http-query details

#!/bin/bash

dest=unzippedResult
doc=template.odt--RESULT

rm -rf $dest
mkdir -p $dest && cp $doc.odt $dest/$doc.zip
unzip -o -u $dest/$doc.zip -d $dest

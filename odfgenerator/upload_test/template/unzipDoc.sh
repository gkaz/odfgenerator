#!/bin/bash

dest=unzipped
doc=template

rm -rf $dest
mkdir -p $dest && cp $doc.odt $dest/$doc.zip
unzip -o -u $dest/$doc.zip -d $dest
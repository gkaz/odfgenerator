#!/bin/bash
echo
clear
#set -x
data_path="./diplom.data.json"
template_path="./diplom.odt"

function q() {
  output_format="$1"
  # set -x
  curl --http1.1 -X POST 'http://localhost:8555/v1.0/' \
  -F "output_format=$output_format" \
  -F "data=@\"$data_path\"" \
  -F "template=@\"$template_path\"" \
  --output "$template_path--RESULT.$output_format"
}

q odt
#q doc
#q docx
#q pdf

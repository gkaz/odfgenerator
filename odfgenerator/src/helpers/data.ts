import {runCMD} from "./cmd";
import * as fs from "fs";
import OdtFiller from "../util/OdtFiller";
import {FillData} from "../interfaces/FillData";


export async function readDataFile(dataFilePath: string): Promise<FillData> {
    try {
        const fileContent = fs.readFileSync(dataFilePath, {
            encoding: 'utf-8',
            flag: 'r'
        });

        const data: FillData = JSON.parse(fileContent);
        if (
            data.parameters
            && data.parameters.length
        ) {
            return data;
        } else {
            throw new Error('Parameters not exists');
        }
    } catch (e) {
        throw new Error('WRONG DATA FILE: ' + JSON.stringify(e));
    }
}

export async function fillParameters(file: string, data: FillData) {
    for (const p of data.parameters) {
        const cmd = [
            'sed', ' ', '-i', ' ', '\'s/{', p.key, '}/', p.value, '/g\'', ' ', file, ' ', '2>/dev/null',
        ].join('');
        try {
            await runCMD(cmd);
        } catch (e) {
            throw new Error(`Error: ${JSON.stringify(e)}`);
        }
    }
    return 'success';
}

export async function fillTables(xmlFile: string, data: FillData): Promise<'success'> {
    const xmlContent = fs.readFileSync(xmlFile, {'encoding': 'utf8'});
    const odtFiller = new OdtFiller(xmlContent);
    odtFiller.fillTables(data.tables);
    fs.writeFileSync(xmlFile, odtFiller.xml);
    return 'success';
}

export async function fillImages(xmlFile: string, data: FillData): Promise<'success'> {
    const xmlContent = fs.readFileSync(xmlFile, {'encoding': 'utf8'});
    const odtFiller = new OdtFiller(xmlContent);
    odtFiller.fillImages(data.images);
    fs.writeFileSync(xmlFile, odtFiller.xml);
    return 'success';
}
import {runCMD} from "./cmd";

export async function testUtilsAvailable(): Promise<'success'> {
    const utils = ['unzip', 'zip', 'sed', 'libreoffice'];
    for (const util of utils) {
        const testCMD = `which ${util}`;
        const testRes = await runCMD(testCMD);
        if (!(testRes.length > 0)) {
            throw new Error(`FATAL ERROR: Util '${util}' does not exist in the system`);
        }
    }
    return 'success';
}

export function pad(num: string, size: number) {
    let s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

export function formatDate(millitime: number) {
    let d = new Date(millitime);
    return d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + d.getDate() + '_' + d.getHours() + '-' + d.getMinutes();
}
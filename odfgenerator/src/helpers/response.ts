import {Response} from "express";

export function sendJsonResponse(res: Response, status: number, content: any) {
    res.status(status);
    res.json(content);
}

import {exec, ExecException} from "child_process";

export async function runCMD(cmd: string, ignoreExceptions = true): Promise<string> {
    return new Promise((resolve, reject) => {
        exec(cmd, (error: ExecException | null, stdout: string) => {
            if (error) {
                if (ignoreExceptions) {
                    resolve(JSON.stringify(error));
                } else {
                    reject(error);
                }
                return;
            }
            resolve(stdout);
        });
    });
}

export async function moveZipToOdt(fileFrom: string, fileTo: string) {
    const cmd = [
        'mv', ' ', '\'', fileFrom, '\'', ' ', '\'', fileTo, '\'', ' ', '2>/dev/null',
    ].join('');
    return runCMD(cmd);
}

export async function createPdfFromOdt(fileFrom: string, dirTo: string) {
    const cmd = [
        'libreoffice', ' ', '--headless', ' ', '--convert-to', ' ',
        'pdf:writer_pdf_Export', ' \'', fileFrom, '\' ', '--outdir', ' \'', dirTo, '\' ', '2>/dev/null',
    ].join('');
    return runCMD(cmd);
}

export async function createDocFromOdt(fileFrom: string, dirTo: string) {
    const cmd = [
        'libreoffice', ' ', '--headless', ' ', '--convert-to', ' ',
        '\'doc:MS Word 97\'', ' \'', fileFrom, '\' ', '--outdir', ' \'', dirTo, '\' ', '2>/dev/null',
    ].join('');
    return runCMD(cmd);
}

export async function createDocxFromOdt(fileFrom: string, dirTo: string) {
    const cmd = [
        'libreoffice', ' ', '--headless', ' ', '--convert-to', ' ',
        '\'docx:MS Word 2007 XML\'', ' \'', fileFrom, '\' ', '--outdir', ' \'', dirTo, '\' ', '2>/dev/null',
    ].join('');
    return runCMD(cmd);
}

export async function unpackZip(file: string, dir: string) {
    const cmd = [
        '[', ' ', '-d', ' ', '\'', dir, '\'', ' ', ']', ' ', '&&', ' ', 'rm', ' ', '-r', ' ', dir, ';',
        'unzip', ' \'', file, '\' ', '-d', ' ', dir, ' ', '2>/dev/null',
    ].join('');
    return runCMD(cmd);
}

export async function packZip(file: string, dir: string) {
    const cmd = [
        'cd', ' ', dir, ';',
        'zip', ' ', '-r', ' \'', file, '\' ', '.', ' ', '2>/dev/null',
    ].join('');
    return runCMD(cmd);
}

export async function cleanup(dir: string) {
    const cmd = ['rm', ' ', '-r', ' ', dir, ' ', '2>/dev/null'];
    return runCMD(cmd.join(''));
}
import {FillData} from "../interfaces/FillData";
import path from "path";
import * as cmdHelper from "./cmd";
import {commonHelper} from "./index";
import {fillImages, fillParameters, fillTables} from "./data";


export type OutputFormat = 'odt' | 'doc' | 'docx' | 'pdf';
export function fillDocument(workDir: string, templateFullPath: string, data: FillData, outfileDirFullPath: string, outputFormat: OutputFormat): Promise<string> {

    let _data: FillData;

    async function _setOutFileFormat(): Promise<'success'> {
        let format = 'odt';
        const acceptedFormats = ['pdf', 'odt', 'doc', 'docx'];
        return new Promise(async (resolve, reject) => {
            if (acceptedFormats.indexOf(outputFormat) === -1) {
                reject(`Unknown output format "${outputFormat}". Setting up "${format}" as default format`);
            } else {
                try {
                    format = outputFormat;
                    const fileFrom = `${outfileDirFullPath}/${path.basename(templateFullPath)}.odt`;
                    const dirTo = outfileDirFullPath;
                    switch (format) {
                        case 'pdf':
                            await cmdHelper.createPdfFromOdt(fileFrom, dirTo);
                            break;
                        case 'doc':
                            await cmdHelper.createDocFromOdt(fileFrom, dirTo);
                            break;
                        case 'docx':
                            await cmdHelper.createDocxFromOdt(fileFrom, dirTo);
                            break;
                    }
                    resolve('success');
                } catch (e) {
                    reject(`Error: ${JSON.stringify(e)}`);
                }
            }
        });
    }

    return new Promise(async (resolve, reject) => {

        try {
            await commonHelper.testUtilsAvailable();
            _data = data;

            // IF NOT CATCHED ERROR THEN START...

            // STEP FIRST
            await cmdHelper.unpackZip(templateFullPath, workDir);

            const file = `${workDir}/content.xml`;

            //FILL TABLES
            if (_data.tables) {
                await fillTables(file, _data);
            }

            // FILL IMAGES
            if (_data.images) {
                await fillImages(file, _data);
            }

            // FILL PARAMETERS
            await fillParameters(file, _data);

            // STEPS LAST
            const zipFile = `${outfileDirFullPath}/${path.basename(templateFullPath)}.zip`;
            const odtFile = `${outfileDirFullPath}/${path.basename(templateFullPath)}.odt`;
            await cmdHelper.packZip(zipFile, workDir);
            await cmdHelper.moveZipToOdt(
                zipFile,
                odtFile
            );

            // SET OUT FILE FORMAT
            await _setOutFileFormat();

            // CLEANUP
            await cmdHelper.cleanup(workDir);

            resolve(`${outfileDirFullPath}/${path.basename(templateFullPath)}.${outputFormat}`);

        } catch (e: unknown) {
            const error = e as Error;
            console.error(error);
            reject(error.toString());
        }
    });
}

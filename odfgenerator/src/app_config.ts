export const config = {
    tmpUploadDir: "/home/ubuntu/workdir/tmpUploadDir",
    workDirBase: "/home/ubuntu/workdir/workDirBase",
    resultsDir: "/home/ubuntu/workdir/resultsDir",
    removeFileOlderThenThisMin: 1,
};

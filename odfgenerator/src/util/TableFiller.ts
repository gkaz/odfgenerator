import {FillDataContent, FillDataTable} from "../interfaces/FillData";

export default class TableFiller {
    public tableElement: Element;
    public document: Document;
    protected ignoreValue = null;

    constructor(tableElement: Element) {
        this.tableElement = tableElement;
        this.document = this.tableElement.ownerDocument;
    }

    get rows() {
        /* returns array with row-nodes from tableNode */
        const elements = this.getChildren(this.tableElement);
        return elements.filter(child => child.tagName === 'table:table-row');
    }

    protected getChildren(node: Node) {
        return Array.from(node.childNodes) as Element[];
    }

    protected setNodeText(node: Node, data: FillDataContent) {
        Array.from(node.childNodes).forEach(node => node.parentNode?.removeChild(node));
        const newTextNode = this.document.createTextNode(String(data));
        node.appendChild(newTextNode);
    }

    protected setCellText(cellNode: Node, data: FillDataContent) {
        const tagTextNode = cellNode.firstChild;
        if (!tagTextNode) {
            throw new Error(`The element does not have a text node`)
        }
        const otherTextNodes = this.getChildren(cellNode).slice(1);
        otherTextNodes.forEach(node => node.parentNode?.removeChild(node));
        this.setNodeText(tagTextNode, data);
    }

    protected fillRow(tableRow: Node, data: FillDataContent[]) {
        /* fills the cells with the row with values from the array  */
        if (!tableRow)
            throw new Error("Bad table row. Object => " + tableRow);

        const cells = this.getChildren(tableRow);
        data.forEach((value, index) => {
            const cell = cells[index];
            if (!cell || value === this.ignoreValue) return;
            this.setCellText(cell, value);
        })
    }

    protected clearRow(tableRow: Node) {
        /* clear every cell from row */
        this.fillRow(tableRow, Array.from({
            length: tableRow.childNodes.length
        }, () => ""));
    }

    protected appendRow() {
        /* append empty row to table */
        const exampleRow = this.rows.slice(-1)[0];
        const newRow = exampleRow.cloneNode(true);
        this.clearRow(newRow);
        this.tableElement.appendChild(newRow);
    }

    protected appendColumn() {
        /* append empty column to table */
        const tableColumn = this.tableElement.firstChild as Element;
        if (!tableColumn) {
            throw new Error(`Table column element does not exists`);
        }

        const colRepeated = +(tableColumn.getAttribute("table:number-columns-repeated") || 1);
        const rows = this.rows;

        rows.forEach((row) => {
            const exampleCell = row.lastChild;
            if (!exampleCell) {
                throw new Error(`Example cell element does not exists`);
            }

            const newCell = exampleCell.cloneNode(true);
            this.setCellText(newCell, "")
            row.appendChild(newCell);
        })

        tableColumn.setAttribute("table:number-columns-repeated", String(colRepeated + 1));
    }

    protected prepareTable(tableData: FillDataTable) {
        /*
        adds missing column and rows
        to successful filling from data templates
        */
        const rows = this.rows;

        const tableRowCount = rows.length;
        const dataRowCount = tableData.body.length;
        const diffRowCount = dataRowCount - tableRowCount + 1; //+1 for header
        for (let i=0; i < diffRowCount; i++)
            this.appendRow();

        const tableColCount = rows[0].childNodes.length;

        const dataColCount  = tableData.head.length;
        const diffColCount = dataColCount - tableColCount;
        for (let i=0; i < diffColCount; i++)
            this.appendColumn();
    }

    protected checkTableData(tableData: FillDataTable) {
        /* init empty body and head if not exists */
        const tableName = tableData.name || "unnamed table";

        if (!tableData.body) {
            console.error(`Body not found in data ${tableName}`);
            tableData.body = [];
        }

        if (!tableData.head) {
            console.error(`Head not found in data ${tableName}`);
            const maxColumn = tableData.body.reduce((prev, cur) => cur.length > prev.length ? cur : prev).length || 0;
            tableData.head = Array.from({length: maxColumn}, () => null);
        }
    }

    public fillHead(headData: FillDataContent[]) {
        /* fills header row by values from data array */
        const rows = this.rows;
        const header = rows[0];
        this.fillRow(header, headData);
    }

    public fillBody(bodyData: FillDataContent[][]) {
        /* fills body rows by values from data array */
        const rows = this.rows.slice(1); //0 - header
        bodyData.forEach((data, index) => this.fillRow(rows[index], data));
    }

    public fillTable(tableData: FillDataTable) {
        /* safe filling head and body */
        this.checkTableData(tableData);
        this.prepareTable(tableData);
        this.fillHead(tableData.head);
        this.fillBody(tableData.body);
    }
}
export default class ImageFiller {
    public imageElement: Element;
    public document: Document;

    constructor(tableElement: Element) {
        this.imageElement = tableElement;
        this.document = this.imageElement.ownerDocument;
    }

    protected getChildren(node: Node) {
        return Array.from(node.childNodes) as Element[];
    }

    public removeImage() {
        const image = this.imageElement.firstChild;
        if (image) {
            this.imageElement.removeChild(image);
        }
    }
    
    public setImage(dataUrl: string) {
        this.removeImage();
        const imageNode = this.createImageNodeBase64(dataUrl);
        this.imageElement.appendChild(imageNode);
    }

    public createImageNodeBase64(dataUrl: string) {
        const regex = /^data:(.+);base64,(.*)$/;
        const matches = dataUrl.match(regex);
        if (!matches) {
            throw new Error(`Некорректные данные для изображения`);
        }
        const mime = matches[1];
        const base64image = matches[2];
        const imageNode = this.document.createElement('draw:image');
        imageNode.setAttribute('draw:mime-type', mime);
        const contentNode = this.document.createElement('office:binary-data');
        const textNode = this.document.createTextNode(base64image);
        contentNode.appendChild(textNode);

        imageNode.appendChild(contentNode);

        return imageNode;
    }
}
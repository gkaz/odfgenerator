import { DOMParser, XMLSerializer } from 'xmldom';
import {FillDataImage, FillDataTable} from "../interfaces/FillData";
import TableFiller from "./TableFiller";
import ImageFiller from "./ImageFiller";


export default class OdtFiller {
    protected document: Document;
    protected tables: HTMLCollectionOf<Element>;
    protected images: HTMLCollectionOf<Element>;

    constructor(xmlContent: string) {
        this.document = new DOMParser().parseFromString(xmlContent);
        this.tables = this.getTables();
        this.images = this.getImages();
    }

    protected getTables() {
        /* returns array-like object with all tables in documents */
        return this.document.documentElement.getElementsByTagName("table:table");
    }

    protected getImages() {
        /* returns array-like object with all tables in documents */
        return this.document.documentElement.getElementsByTagName("draw:frame");
    }

    protected getTableByName(tableName: string) {
        /* returns tableNode */
        for (const table of Array.from(this.tables)) {
            if (table.getAttribute('table:name') === tableName) {
                return table;
            }
        }
    }

    protected getImageByName(name: string) {
        /* returns imageNode */
        for (const image of Array.from(this.images)) {
            if (image.getAttribute('draw:name') === name) {
                return image;
            }
        }
    }

    public fillTables(tablesData: FillDataTable[]) {
        if (!tablesData.forEach)
            throw new Error('tables not array');

        tablesData.forEach((tableData, index) => {
            const tableName = tableData.name;
            if (!tableName) {
                throw new Error(`Template name not found for table with index ${index}`);
            }

            const tableNode = this.getTableByName(tableName);
            if (!tableNode) {
                throw new Error(`Table ${tableName} not found in document`);
            }

            const tableFiller = new TableFiller(tableNode);
            tableFiller.fillTable(tableData);
        });
    }

    public fillImages(imagesData: FillDataImage[]) {
        if (!Array.isArray(imagesData)) {
            throw new Error('images not array');
        }

        imagesData.forEach((tableData, index) => {
            const imageName = tableData.name;
            if (!imageName) {
                throw new Error(`Template name not found for table with index ${index}`);
            }

            const imageNode = this.getImageByName(imageName);
            if (!imageNode) {
                throw new Error(`Image ${imageName} not found in document`);
            }

            const imageFiller = new ImageFiller(imageNode);
            try {
                imageFiller.setImage(tableData.data);
            } catch (e: unknown) {
                const error = e as Error;

                throw new Error(`Произошла ошибка при обработке изображения ${tableData.name}:\n${error.message}`);
            }
        });
    }

    get xml() {
        return new XMLSerializer().serializeToString(this.document);
    }
}
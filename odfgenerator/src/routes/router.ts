import express, {Response} from "express";
import multer from 'multer'
import * as fs from "fs";
import { config } from "../app_config";
import { exec } from 'child_process';

import * as ctrl from '../controllers/controller';


function clearDirs() {
    if (fs.existsSync(config.tmpUploadDir)) {
        fs.rmSync(config.tmpUploadDir, {recursive: true});
    }

    if (fs.existsSync(config.resultsDir)) {
        fs.rmSync(config.resultsDir, {recursive: true});
    }

    if (fs.existsSync(config.workDirBase)) {
        fs.rmSync(config.workDirBase, {recursive: true});
    }
}

function createDir() {
    // create destination dir if not exist
    if (!fs.existsSync(config.tmpUploadDir)) {
        fs.mkdirSync(config.tmpUploadDir);
    }

    // create destination dir if not exist
    if (!fs.existsSync(config.resultsDir)) {
        fs.mkdirSync(config.resultsDir);
    }

    // create destination dir if not exist
    if (!fs.existsSync(config.workDirBase)) {
        fs.mkdirSync(config.workDirBase);
    }

}

async function deleteFilesOlderThenXDays(x: number) {
    try {
        await Promise.all([
            exec(`find ${config.tmpUploadDir} -mmin +${x} -type f -exec rm -f {} \\;`),
            exec(`find ${config.resultsDir} -mmin +${x} -type f -exec rm -f {} \\;`),
            exec(`find ${config.workDirBase} -mmin +${x} -type f -exec rm -f {} \\;`),
        ]);
    } catch (e) {
        console.log('wtf?', e);
    }
}

// on boot
clearDirs();
createDir();

setInterval(async () => {
    // watch
    createDir();

    //
    await deleteFilesOlderThenXDays(config.removeFileOlderThenThisMin);

}, 5000);

const upload = multer({dest: config.tmpUploadDir});


const router = express.Router();

router.post(
    '/',
    upload.fields([{name: 'data', maxCount: 1}, {name: 'template', maxCount: 1}]),
    ctrl.upload
);

export {
    router
}

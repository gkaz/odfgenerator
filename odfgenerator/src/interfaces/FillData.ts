export type FillDataContent = string | number | null;

export interface FillData {
    parameters: FillDataParameter[];
    tables: FillDataTable[];
    images: FillDataImage[];
}

export interface FillDataParameter {
    key: string;
    value: string;
}

export interface FillDataTable {
    name: string;
    title: string;
    head: FillDataContent[];
    body: FillDataContent[][];
}

export interface FillDataImage {
    name: string;
    data: string;
    qrText: string;
}

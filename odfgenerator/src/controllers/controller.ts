import {config} from "../app_config";
import * as fs from "fs";
import {Request, Response, Express} from 'express';
import {OutputFormat} from "../helpers/document";
import {dataHelper, documentHelper, responseHelper} from '../helpers';
import {FillData} from "../interfaces/FillData";

interface UploadFiles {
    data?: Express.Multer.File[];
    template?: Express.Multer.File[];
}

interface ReqBody {
    output_format: OutputFormat;
    data: string;
}

export async function upload(req: Request, res: Response) {
    const uploadedFiles = req.files as UploadFiles;
    const bodyParams: ReqBody = req.body;
    let outputFormat: OutputFormat = 'odt';

    if (bodyParams.output_format && bodyParams.output_format.length){
        outputFormat = bodyParams.output_format;
    }

    try {
        const dataFile = uploadedFiles.data?.[0];

        const fillData = dataFile ?
            await dataHelper.readDataFile(dataFile.path)
            : JSON.parse(bodyParams.data) as FillData;

        if (!fillData) {
            responseHelper.sendJsonResponse(res, 500, {status: 'error', error: 'data for template is not found'});
            return;
        }

        const template = uploadedFiles.template?.[0];
        if (!template) {
            responseHelper.sendJsonResponse(res, 500, {status: 'error', error: 'template is not found'});
            return;
        }

        const dataFullPath = dataFile?.path;
        const dataFilename = dataFile?.filename;

        const templateFullPath = template.path;
        const templateFilename = template.filename;
        const uniqTmpDirName = `${dataFilename || 'dataObject'}_${templateFilename}`;
        const outfileDirFullPath = config.resultsDir;
        const workDir = `${config.workDirBase}/${uniqTmpDirName}`;

        const filename = await documentHelper.fillDocument(
            workDir,
            templateFullPath,
            fillData,
            outfileDirFullPath,
            outputFormat
        );

        res.sendFile(filename);

        fs.unlinkSync(templateFullPath);

        if (dataFullPath) {
            fs.unlinkSync(dataFullPath);
        }

    } catch (e) {
        console.error(e);
        responseHelper.sendJsonResponse(res, 500, {status: 'error', error: e});
    }
}
import express from 'express';
import { router } from "./routes/router";

const app = express();
app.use('/v1.0', router);

export default app;
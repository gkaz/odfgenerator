#!/bin/bash
PATH="$PATH:/opt/nodejs/bin"
export PORT=8555
cd /home/ubuntu/app

if [ ! -d node_modules ]; then
  echo 'node_modules not exist. npm install...'
  npm install
fi

echo compile
npx tsc
echo start server
node --inspect=0.0.0.0:9230 dist/bin/www.js
